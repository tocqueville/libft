#include "test.h"

#define TEST_CASE(a, b, ref) do {	\
	int	__ret;	\
	if ((__ret = do_test(a, b, ft_##ref, ref)))	\
		ft_printf(#ref": {red}DIFF KO{}\n");	\
	else	\
		ft_printf(#ref": {green}DIFF OK{}\n");	\
	ret |= __ret;	\
} while (0)

int	do_test(int a, int b, int (*f)(int), int (*ref)(int))
{
	for (int i = a; i < b; i++)
	{
		if (!f(i) != !ref(i))
		{
			ft_printf("%d: output is '%d', expected '%d'\n", i, f(i), ref(i));
			return (1);
		}
	}
	return (0);
}

int	chr_test()
{
	int ret = 0;

	TEST_CASE(0, 256, isalnum);
	TEST_CASE(0, 256, isalpha);
	TEST_CASE(0, 256, isascii);
	TEST_CASE(0, 256, isdigit);
	TEST_CASE(0, 256, isprint);
	TEST_CASE(0, 256, isspace);
	TEST_CASE(0, 256, tolower);
	TEST_CASE(0, 256, toupper);

	return (ret);
}

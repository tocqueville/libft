#include "test.h"
#include "libft.h"

static void	printer(char *key, char *val)
{
	ft_printf("%s=%s\n", key, val);
}

int	map_test()
{
	int ret = 0;
	t_map *map = ft_mapnew(75, NULL);

	ft_mapadd(map, ft_strdup("cle"), "valeur");
	ft_mapadd(map, ft_strdup("tom"), "jerry");
	ft_mapadd(map, ft_strdup("patrick"), "timsit");
	ft_mapadd(map, ft_strdup("john"), "marston");
	ft_mapadd(map, ft_strdup("neo"), "anderson");
	ft_mapadd(map, ft_strdup("virtus"), "pro");
	ft_mapadd(map, ft_strdup("sk"), "gaming");
	ft_mapadd(map, ft_strdup("team"), "liquid");
	ft_mapadd(map, ft_strdup("richard"), "stallman");
	ft_mapadd(map, ft_strdup("linus"), "torvalds");
	ft_mapadd(map, ft_strdup("bill"), "gates");
	ft_mapadd(map, ft_strdup("m4"), "a1s");
	ft_mapadd(map, ft_strdup("tant va"), "la cruche a l'eau");
	ft_mapadd(map, ft_strdup("tout est bien"), "qui finit bien");

	if (ft_strcmp("valeur", ft_mapget(map, "cle")))
		ret = 1;

	ft_printf("My map contains: \n");
	ft_mapiter(map, printer);
	ft_printf("'%ld' values (expects '14')\n", map->count);
	ret |= map->count != 14;

	ft_mapfree(map);

	return (ret);
}

#include "test.h"
#include "libft.h"

static void	printer(char *val)
{
	ft_printf("%s\n", val);
}

static void	*dup(void *val)
{
	char *str;

	str = val;
	return (ft_strdup(str));
}

int	arr_test()
{
	int ret = 0;
	size_t i;

	t_array *arr = ft_arrnew(3, NULL);

	ft_arrpush(arr, "valeur");
	ft_arrpush(arr, "jerry");
	ft_arrpush(arr, "timsit");
	ft_arrpush(arr, "marston");
	ft_arrpush(arr, "anderson");
	ft_arrpush(arr, "liquid");

	ft_arrinsert(arr, 0, "stallman");
	ft_arrinsert(arr, 3, "torvalds");
	ft_arrinsert(arr, 12, "gates");

	ft_printf("Moving 'timsit' to last position\n");
	if ((ft_arrfind(arr, "timsit", ft_strcmp, &i)))
	{
		ft_printf("{red}ERROR:{} Element 'timsit' not found\n");
		ret = 1;
	}
	else
		ft_arrremove(arr, i);
	ft_arrpush(arr, "timsit");

	ft_printf("'%ld' values (expects '9')\n", arr->size);
	ft_printf("arr contains: \n");
	ft_arriter(arr, printer);

	ft_arrsort(arr, ft_strcmp);
	ft_printf("\nafter sort, arr contains: \n");
	ft_arriter(arr, printer);

	if ((ft_arrfind(arr, "jerry", ft_strcmp, &i)))
	{
		ft_printf("{red}ERROR:{} Element 'jerry' not found\n");
		ret = 1;
	}
	else
		ft_printf("\nindex of 'jerry' is: '%ld'\n", i);

	ret = arr->size != 9;
	if (arr->data[arr->size])
		ret = 1;

	char *cmd = "   find   *.c  -exec  vim {} -c \"normal O\" \\; && (echo \"bonjour a tous\")  ";
	t_array *args = ft_arrnew(5, free);
	ft_arrtokenize(args, cmd);

	ft_printf("\ncmd contains: (src is '%s')\n", cmd);
	ft_arriter(args, printer);

	ft_printf("\ncpy contains: (shall be a copy of cmd)\n");
	t_array *cpy = ft_arrmap(args, dup);
	ft_arriter(cpy, printer);

	if (cpy->size != args->size)
		ret = 1;

	ft_arrfree(arr);
	ft_arrfree(args);
	ft_arrfree(cpy);
	return (ret);
}

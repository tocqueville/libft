#include "test.h"

int	lst_test();
int	map_test();
int	chr_test();
int	str_test();
int	arr_test();

#define TEST(module) do {	\
	ft_printf(" ==== TESTING MODULE {yellow}"#module"{} === \n");	\
	if (module##_test())	\
		ft_printf("{red}DIFF KO{}\n\n");	\
	else	\
		ft_printf("{green}DIFF OK{}\n\n");	\
} while (0)

int	main(int argc, char **argv)
{
	TEST(arr);
	TEST(map);
	//TEST(lst);
	TEST(chr);
	TEST(str);
	return (0);
}

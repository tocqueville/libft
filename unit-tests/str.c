#include "test.h"
#include <stdio.h>

#define TEST_CASE(tester) do {	\
	int	__ret;	\
	if ((__ret = tester()))	\
		ft_printf(#tester": {red}DIFF KO{}\n\n\n");	\
	else	\
		ft_printf(#tester": {green}DIFF OK{}\n\n\n");	\
	ret |= __ret;	\
} while (0)

int	test_strtok()
{
	char *cmd = "   find   *.c  -exec  vim {} -c \"normal O\" \\; && (echo \"bonjour a tous\")  ";
	char *cur, *tok;
	int count = 0;

	cur = cmd;
	printf("cmd: '%s' @%p\n", cur, cur);
	while ((tok = ft_strtok(&cur)))
	{
			tok = ft_strsub(tok, 0, cur - tok);
			printf("'%s'\n", tok);
			free(tok);
			count++;
	}
	return (count != 10);
}

int	test_strsplitarr()
{
	char *str = "chemin/vers/un/fichier/sympa";
	size_t i;

	t_array *arr = ft_strsplitarr(str, '/');
	printf("str: '%s' (%ld)\n", str, arr->size);
	for (i = 0; i < arr->size; i++)
	{
		printf("%s\n", (char*)arr->data[i]);
	}
	i = arr->size;
	ft_arrfree(arr);
	return (i != 5);
}

int	str_test()
{
	int ret = 0;

	TEST_CASE(test_strtok);
	TEST_CASE(test_strsplitarr);

	return (ret);
}

#include "test.h" 	


int	test_lst()
{
	t_list *l = lstnew(strdup(" 1 2 3 "), 8);
	t_list *ret;

	l->next = lstnew(strdup("ss"), 3);
	l->next->next = lstnew(strdup("-_-"), 4);
	ret = ft_lstmap(l, lstmap_f);
	if (!strcmp(ret->content, "OK !") && !strcmp(ret->next->content, "OK !") && !strcmp(ret->next->next->content, "OK !") && !strcmp(l->content, " 1 2 3 ") && !strcmp(l->next->content, "ss") && !strcmp(l->next->next->content, "-_-"))
		exit(TEST_SUCCESS);
	exit(TEST_FAILED);
}

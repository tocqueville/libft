VPATH += chr/
SRC_CHR = ft_isalnum.c ft_isalpha.c ft_isascii.c ft_isdigit.c ft_isprint.c \
	ft_isspace.c ft_tolower.c ft_toupper.c

OBJ += $(SRC_CHR:.c=.o)
HEADER += chr/ft_char.h
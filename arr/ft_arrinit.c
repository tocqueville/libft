/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrinit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:00:58 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 14:00:58 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"

int	ft_arrinit(t_array *array, size_t capacity, void (*dtor)())
{
	if (!(array->data = malloc((capacity + 1) * sizeof(void *))))
		return (-1);
	array->data[0] = NULL;
	array->size = 0;
	array->capacity = capacity;
	array->dtor = dtor;
	return (0);
}

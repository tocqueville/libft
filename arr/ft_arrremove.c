/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrremove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:00:58 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 14:00:58 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"

int	ft_arrremove(t_array *array, size_t index)
{
	size_t i;

	if (index >= array->size)
		return (ft_arrpop(array));
	if (array->dtor)
		array->dtor(array->data[index]);
	array->size--;
	i = index;
	while (i < array->size)
	{
		array->data[i] = array->data[i + 1];
		i++;
	}
	array->data[array->size] = NULL;
	return (0);
}

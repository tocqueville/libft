/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrfind.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 08:29:42 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/14 08:38:18 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"

int	ft_arrfind(t_array *array, void *value, int (*cmp)(), size_t *index)
{
	size_t i;

	i = 0;
	while (i < array->size)
	{
		if (cmp(array->data[i], value) == 0)
		{
			*index = i;
			return (0);
		}
		i++;
	}
	return (1);
}

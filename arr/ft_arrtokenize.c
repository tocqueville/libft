/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrtokenize.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:04:54 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:06:49 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"
#include "../str/ft_string.h"
#include "../mem/ft_memory.h"

static char	*next_token(char *str)
{
	while (*str == ' ')
		str++;
	return (str);
}

static void	update_stacks(size_t stacks[3], char c)
{
	if (c == '(')
		stacks[0]++;
	else if (c == ')')
		stacks[0]--;
	else if (c == '\'')
		stacks[1]++;
	else if (c == '\"')
		stacks[2]++;
}

static char	*parse_token(char *str)
{
	size_t stacks[3];

	ft_memset(stacks, 0, sizeof(stacks));
	while (*str)
	{
		if (*str == ' ')
		{
			if (!stacks[0] && !(stacks[1] % 2) && !(stacks[2] % 2))
				return (str);
		}
		else
			update_stacks(stacks, *str);
		str++;
	}
	if (!stacks[0] && !(stacks[1] % 2) && !(stacks[2] % 2))
		return (str);
	return (NULL);
}

int			ft_arrtokenize(t_array *array, char *str)
{
	char *start;
	char *end;

	if (array == NULL)
		return (0);
	while (*str)
	{
		start = next_token(str);
		end = parse_token(start);
		if (start == end)
			break ;
		if (ft_arrpush(array, ft_strsub(start, 0, end - start)))
		{
			ft_arrclear(array);
			return (0);
		}
		str = end;
	}
	return (1);
}

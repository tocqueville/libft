/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrpush.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:00:58 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 14:00:58 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"

int	ft_arrpush(t_array *array, void *value)
{
	if (array->size == array->capacity)
	{
		if (ft_arrreserve(array, array->capacity + 5))
			return (1);
	}
	array->data[array->size++] = value;
	array->data[array->size] = NULL;
	return (0);
}

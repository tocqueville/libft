/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrreserve.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:00:58 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 14:00:58 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"

int	ft_arrreserve(t_array *array, size_t capacity)
{
	void	**new;
	size_t	i;

	if (capacity <= array->capacity)
		return (0);
	if (!(new = malloc((capacity + 1) * sizeof(void *))))
		return (1);
	i = 0;
	while (i < array->size)
	{
		new[i] = array->data[i];
		i++;
	}
	new[array->size] = NULL;
	free(array->data);
	array->data = new;
	array->capacity = capacity;
	return (0);
}

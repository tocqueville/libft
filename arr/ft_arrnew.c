/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:00:58 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 14:00:58 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"

t_array	*ft_arrnew(size_t capacity, void (*dtor)())
{
	t_array	*arr;

	if (!(arr = malloc(sizeof(*arr))))
		return (NULL);
	if (ft_arrinit(arr, capacity, dtor))
	{
		free(arr);
		return (NULL);
	}
	return (arr);
}

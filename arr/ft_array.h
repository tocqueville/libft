/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_array.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:03:50 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/14 08:37:49 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ARRAY_H
# define FT_ARRAY_H

# include <stdlib.h>
# include <string.h>

/*
** This struct can represent a variable-length null-terminated array of pointers
*/

typedef struct	s_array
{
	void	**data;
	size_t	size;
	size_t	capacity;
	void	(*dtor)();
}				t_array;

t_array			*ft_arrnew(size_t capacity, void (*dtor)());
void			ft_arrfree(t_array *array);

int				ft_arrinit(t_array *array, size_t capacity, void (*dtor)());
void			ft_arrclear(t_array *array);
void			ft_arrdestroy(t_array *array);

int				ft_arrresize(t_array *array, size_t size, void *value);
int				ft_arrreserve(t_array *array, size_t capacity);

int				ft_arrpush(t_array *array, void *value);
int				ft_arrpop(t_array *array);
int				ft_arrinsert(t_array *array, size_t index, void *value);
int				ft_arrremove(t_array *array, size_t index);

int				ft_arrfind(t_array *array, void *value, int (*cmp)(),
		size_t *index);
int				ft_arrtokenize(t_array *array, char *str);
void			ft_arriter(t_array *array, void (*f)());
t_array			*ft_arrmap(t_array *array, void *(*f)(void *));
void			ft_arrsort(t_array *array, int (*cmp)());

#endif

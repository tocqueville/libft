/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:00:58 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 14:00:58 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"

t_array	*ft_arrmap(t_array *array, void *(*f)(void *))
{
	t_array	*res;
	size_t	i;

	i = 0;
	if (!(res = ft_arrnew(array->capacity, array->dtor)))
		return (NULL);
	while (i < array->size)
	{
		if (f)
			res->data[i] = f(array->data[i]);
		else
			res->data[i] = array->data[i];
		i++;
	}
	res->data[i] = NULL;
	res->size = array->size;
	return (res);
}

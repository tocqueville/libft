/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrinsert.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:00:58 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 14:06:50 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"

static int	push_insert(t_array *array, size_t index, void *value)
{
	void	**new;
	size_t	i;
	size_t	capacity;

	capacity = array->capacity + 5;
	if (!(new = malloc((capacity + 1) * sizeof(void *))))
		return (1);
	i = 0;
	while (i < index)
	{
		new[i] = array->data[i];
		i++;
	}
	new[index] = value;
	while (i < array->size)
	{
		new[i + 1] = array->data[i];
		i++;
	}
	array->size++;
	new[array->size] = NULL;
	free(array->data);
	array->data = new;
	array->capacity = capacity;
	return (0);
}

int			ft_arrinsert(t_array *array, size_t index, void *value)
{
	size_t i;

	if (index >= array->size)
		return (ft_arrpush(array, value));
	if (array->size == array->capacity)
		return (push_insert(array, index, value));
	i = array->size;
	while (i > index)
	{
		array->data[i] = array->data[i - 1];
		i--;
	}
	array->size++;
	array->data[index] = value;
	array->data[array->size] = NULL;
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:04:44 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:05:58 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"

static void	ft_swap(void **a, void **b)
{
	void *temp;

	temp = *a;
	*a = *b;
	*b = temp;
}

void		ft_arrsort(t_array *array, int (*cmp)())
{
	size_t i;
	size_t j;
	size_t min;

	i = 0;
	while (i < array->size)
	{
		min = i;
		j = i;
		while (++j < array->size)
		{
			if (cmp(array->data[j], array->data[min]) < 0)
				min = j;
		}
		ft_swap(array->data + i, array->data + min);
		i++;
	}
}

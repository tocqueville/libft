VPATH += arr/
SRC_ARR = ft_arrnew.c ft_arrinit.c ft_arrfree.c ft_arrclear.c ft_arrdestroy.c \
	ft_arrresize.c ft_arrreserve.c \
	ft_arrpush.c ft_arrpop.c ft_arrinsert.c ft_arrremove.c \
	ft_arrfind.c ft_arriter.c ft_arrmap.c ft_arrtokenize.c ft_arrsort.c

OBJ += $(SRC_ARR:.c=.o)
HEADER += arr/ft_array.h

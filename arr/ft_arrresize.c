/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrresize.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:00:58 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 14:00:58 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_array.h"

int	ft_arrresize(t_array *array, size_t size, void *value)
{
	if (size < array->size)
	{
		if (!array->dtor)
			array->size = size;
		while (array->size > size)
			array->dtor(array->data[--array->size]);
	}
	else if (size > array->capacity)
	{
		if (ft_arrreserve(array, size))
			return (1);
	}
	else
	{
		while (array->size < size)
			array->data[array->size++] = value;
	}
	array->data[size] = NULL;
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 10:34:04 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/14 08:43:20 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <sys/types.h>
# include "../libft.h"

# define BUFF_SIZE 8

typedef struct	s_buf
{
	int		fd;
	ssize_t	end;
	char	buf[BUFF_SIZE];
}				t_buf;

int				get_next_line(const int fd, char **line);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 10:34:00 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/14 09:24:47 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "get_next_line.h"

static t_buf	*get_buf(t_list **bufs, const int fd)
{
	t_list		*iter;
	t_buf		buf;

	if (fd < 0)
		return (NULL);
	iter = *bufs;
	while (iter)
	{
		if (((t_buf *)iter->content)->fd == fd)
			return (iter->content);
		iter = iter->next;
	}
	buf.fd = fd;
	buf.end = 0;
	ft_memset(buf.buf, 0, BUFF_SIZE);
	if (!(iter = ft_lstnew(&buf, sizeof(t_buf))))
		return (NULL);
	iter->next = *bufs;
	*bufs = iter;
	return (iter->content);
}

static void		remove_buf(t_list **bufs, t_buf *buf)
{
	t_list	*iter;
	t_list	*elem;

	iter = *bufs;
	if (iter == NULL)
		return ;
	if (iter->content == buf)
	{
		*bufs = iter->next;
		free(iter);
		free(buf);
		return ;
	}
	while (iter->next)
	{
		if (iter->next->content == buf)
		{
			elem = iter->next;
			iter->next = elem->next;
			free(elem);
			free(buf);
			return ;
		}
		iter = iter->next;
	}
}

static int		push_back(char **line, size_t *len, t_buf *buf)
{
	ssize_t	i;

	if (!(*line = ft_memrealloc(*line, *len, *len + buf->end + 1)))
		return (-1);
	i = 0;
	while (i < buf->end)
	{
		if (buf->buf[i] == '\n')
		{
			buf->end -= (i + 1);
			ft_memmove(buf->buf, buf->buf + i + 1, buf->end);
			(*line)[*len] = '\0';
			return (1);
		}
		(*line)[(*len)++] = buf->buf[i++];
	}
	(*line)[*len] = 0;
	buf->end = 0;
	return (0);
}

static int		clean_up(t_list **bufs, t_buf *buf, ssize_t bytes, char **line)
{
	if (bytes == -1)
	{
		free(*line);
		*line = NULL;
		return (-1);
	}
	if (bytes == 0)
		remove_buf(bufs, buf);
	if (*line)
		return (1);
	return (0);
}

int				get_next_line(const int fd, char **line)
{
	static t_list	*bufs = NULL;
	t_buf			*buf;
	ssize_t			bytes;
	size_t			length;
	int				ret;

	if (!(buf = get_buf(&bufs, fd)) || !line)
		return (-1);
	*line = NULL;
	length = 0;
	bytes = 1;
	while (bytes > 0)
	{
		if (buf->end && (ret = push_back(line, &length, buf)))
			return (ret);
		if ((bytes = read(fd, buf->buf, BUFF_SIZE)) > 0)
			buf->end = bytes;
	}
	return (clean_up(&bufs, buf, bytes, line));
}

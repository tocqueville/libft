/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:08:57 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/14 08:45:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	get_max_power(int value, int *p)
{
	int n;

	*p = 1;
	n = 1;
	while (value / 10 >= *p || value / 10 <= -*p)
	{
		*p *= 10;
		n++;
	}
	return (n);
}

char		*ft_itoa(int n)
{
	int		i;
	int		len;
	int		p;
	char	*res;

	i = 0;
	len = get_max_power(n, &p) + (n < 0);
	if (!(res = malloc(len + 1)))
		return (NULL);
	if (n < 0)
		res[i++] = '-';
	while (i < len)
	{
		res[i] = '0' + (n < 0 ? -1 : 1) * (n / p);
		n = n % p;
		p /= 10;
		i++;
	}
	res[i] = 0;
	return (res);
}

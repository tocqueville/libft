/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mapnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:11:19 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:11:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map.h"
#include "../mem/ft_memory.h"

t_map	*ft_mapnew(size_t factor, void (*dtor)())
{
	t_map *map;

	if (!(map = malloc(sizeof(t_map))))
		return (NULL);
	if (!(map->buckets = ft_memalloc(16 * sizeof(t_elem *))))
	{
		free(map);
		return (NULL);
	}
	map->size = 16;
	map->count = 0;
	map->factor = factor;
	map->dtor = dtor;
	return (map);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map_internals.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:12:39 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:12:53 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MAP_INTERNALS_H
# define FT_MAP_INTERNALS_H

# include "ft_map.h"

typedef unsigned long t_hash;

int		ft_mapresize(t_map *map);

void	ft_elemfree(t_elem *elem, void (*dtor)());
void	ft_elemadd(t_elem **e, t_elem *new);
t_elem	*ft_elemget(t_map *map, const char *key, t_hash *h);

t_hash	ft_maphash(const char *key);

#endif

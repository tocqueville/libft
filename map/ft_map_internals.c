/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map_internals.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:11:19 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:13:21 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map_internals.h"
#include "../str/ft_string.h"

t_hash	ft_maphash(const char *str)
{
	t_hash	hash;
	int		c;

	hash = 5381;
	while ((c = *str++))
		hash = ((hash << 5) + hash) + c;
	return (hash);
}

t_elem	*ft_elemget(t_map *map, const char *key, t_hash *h)
{
	t_elem	*e;

	*h = ft_maphash(key) % map->size;
	e = map->buckets[*h];
	while (e)
	{
		if (ft_strequ(e->key, key))
			return (e);
		e = e->next;
	}
	return (NULL);
}

void	ft_elemadd(t_elem **e, t_elem *new)
{
	new->next = *e;
	*e = new;
}

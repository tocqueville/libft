/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mapclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:11:19 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:11:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map_internals.h"

void	ft_elemfree(struct s_elem *elem, void (*dtor)())
{
	struct s_elem *next;

	while (elem)
	{
		next = elem->next;
		if (dtor)
			dtor(elem->value);
		free(elem->key);
		free(elem);
		elem = next;
	}
}

void	ft_mapclear(t_map *map)
{
	size_t i;

	i = 0;
	while (i < map->size)
	{
		if (map->buckets[i])
			ft_elemfree(map->buckets[i], map->dtor);
		map->buckets[i] = NULL;
		i++;
	}
	map->count = 0;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mapiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:11:19 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:11:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map.h"

void	ft_mapiter(t_map *map, void (*f)())
{
	size_t	i;
	t_elem	*e;

	i = 0;
	while (i < map->size)
	{
		e = map->buckets[i];
		while (e)
		{
			if (e->value)
				f(e->key, e->value);
			e = e->next;
		}
		i++;
	}
}

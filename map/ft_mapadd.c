/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mapadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:11:19 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:11:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map_internals.h"

int	ft_mapadd(t_map *map, char *key, void *value)
{
	t_hash	h;
	t_elem	*e;

	e = ft_elemget(map, key, &h);
	if (e)
	{
		free(e->key);
		if (map->dtor)
			map->dtor(e->value);
		e->key = key;
		e->value = value;
		return (0);
	}
	if (map->count * 100 > map->size * map->factor)
		if (ft_mapresize(map))
			return (-1);
	if (!(e = malloc(sizeof(t_elem))))
		return (-1);
	e->key = key;
	e->value = value;
	ft_elemadd(&(map->buckets[h]), e);
	map->count++;
	return (0);
}

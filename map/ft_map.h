/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:11:26 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:12:29 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MAP_H
# define FT_MAP_H

# include <stdlib.h>
# include <string.h>

typedef struct	s_elem
{
	char			*key;
	void			*value;
	struct s_elem	*next;
}				t_elem;

typedef struct	s_map
{
	t_elem	**buckets;
	size_t	size;
	size_t	count;
	size_t	factor;
	void	(*dtor)();
}				t_map;

t_map			*ft_mapnew(size_t factor, void (*dtor)());
void			ft_mapfree(t_map *map);

int				ft_mapadd(t_map *map, char *key, void *value);
int				ft_mapremove(t_map *map, const char *key);
void			*ft_mapget(t_map *map, const char *key);

void			ft_mapclear(t_map *map);

void			ft_mapiter(t_map *map, void (*f)());

#endif

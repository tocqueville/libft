/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mapresize.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:11:19 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:11:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map_internals.h"
#include "../mem/ft_memory.h"

int	ft_mapresize(t_map *map)
{
	t_elem	**new;
	t_elem	*e;
	t_elem	*next;
	t_hash	h;
	size_t	i;

	if (!(new = ft_memalloc(map->size * 2 * sizeof(t_elem *))))
		return (-1);
	i = 0;
	while (i < map->size)
	{
		e = map->buckets[i];
		while (e)
		{
			next = e->next;
			h = ft_maphash(e->key);
			ft_elemadd(&(new[h % (map->size * 2)]), e);
			e = next;
		}
		i++;
	}
	map->size *= 2;
	free(map->buckets);
	map->buckets = new;
	return (0);
}

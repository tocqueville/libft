/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mapremove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:11:19 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:11:19 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map_internals.h"

int	ft_mapremove(t_map *map, const char *key)
{
	t_hash	h;
	t_elem	*e;

	e = ft_elemget(map, key, &h);
	if (!e)
		return (1);
	if (map->dtor)
		map->dtor(e->value);
	e->value = NULL;
	return (0);
}

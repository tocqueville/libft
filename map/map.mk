VPATH += map/
SRC_ARR = ft_mapadd.c ft_mapclear.c ft_mapfree.c ft_mapget.c ft_mapiter.c \
	ft_mapnew.c ft_mapremove.c ft_mapresize.c ft_map_internals.c

OBJ += $(SRC_ARR:.c=.o)
HEADER += map/ft_map.h map/ft_map_internals.h

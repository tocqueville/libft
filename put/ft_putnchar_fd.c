/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnchar_fd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:14:09 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:14:09 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_putnchar_fd(int n, char c, int fd)
{
	int bytes;

	bytes = 0;
	while (n-- > 0)
		bytes += write(fd, &c, 1);
	return (bytes);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:14:09 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:14:09 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../str/ft_string.h"
#include <unistd.h>

void	ft_putstr_fd(char const *s, int fd)
{
	int len;

	if (s && (len = ft_strlen(s)))
		write(fd, s, len);
}

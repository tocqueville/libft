VPATH += put/
SRC_PUT = ft_putchar.c ft_putchar_fd.c ft_putnchar_fd.c ft_putendl.c \
	ft_putendl_fd.c ft_putnbr.c ft_putnbr_fd.c ft_putnchar_fd.c \
	ft_putstr.c ft_putstr_fd.c

OBJ += $(SRC_PUT:.c=.o)
HEADER += put/ft_put.h
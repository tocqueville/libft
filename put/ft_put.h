/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:14:27 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:14:32 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PUT_H
# define FT_PUT_H

void	ft_putchar(char c);
void	ft_putchar_fd(char c, int fd);
int		ft_putnchar_fd(int n, char c, int fd);

void	ft_putstr(char const *str);
void	ft_putstr_fd(char const *s, int fd);

void	ft_putendl(char const *str);
void	ft_putendl_fd(char const *s, int fd);

void	ft_putnbr(int n);
void	ft_putnbr_fd(int n, int fd);

#endif

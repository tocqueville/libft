VPATH += mem/
SRC_MEM := ft_bzero.c ft_memalloc.c ft_memrealloc.c ft_memccpy.c ft_memchr.c \
	ft_memcmp.c ft_memcpy.c ft_memdel.c ft_memmove.c ft_memset.c

OBJ += $(SRC_MEM:.c=.o)
HEADER += mem/ft_memory.h

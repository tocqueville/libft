/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memrealloc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:37:00 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 14:13:37 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_memory.h"

void	*ft_memrealloc(void *buf, size_t len, size_t new_len)
{
	void	*temp;

	if (!(temp = malloc(new_len)))
		return (NULL);
	if (buf)
		ft_memcpy(temp, buf, len);
	free(buf);
	return (temp);
}

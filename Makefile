NAME = libft.a
CC = gcc
CFLAGS = -Wall -Wextra -Werror

OBJ := ft_atoi.o ft_itoa.o
HEADER :=  libft.h

include arr/arr.mk
include chr/chr.mk
include lst/lst.mk
include map/map.mk
include mem/mem.mk
include put/put.mk
include str/str.mk

include gnl/gnl.mk

OBJDIR = obj/
OBJ := $(addprefix $(OBJDIR), $(OBJ))

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)

$(OBJDIR)%.o: %.c $(HEADERS)
	@mkdir -p $(OBJDIR)
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -rf $(OBJDIR)

fclean: clean
	rm -f $(NAME)

re: fclean all

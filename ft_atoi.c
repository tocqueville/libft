/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:08:50 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/14 08:45:09 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *nptr)
{
	int		sign;
	long	res;
	long	new_res;

	res = 0;
	while (ft_isspace(*nptr))
		nptr++;
	sign = *nptr == '-' ? -1 : 1;
	if (*nptr == '-' || *nptr == '+')
		nptr++;
	while (*nptr >= '0' && *nptr <= '9')
	{
		new_res = res * 10 + sign * (*nptr - '0');
		if (new_res > 0 && res < 0)
			return (0);
		if (new_res < 0 && res > 0)
			return (-1);
		res = new_res;
		nptr++;
	}
	return (res);
}

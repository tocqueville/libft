/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:07:52 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:08:12 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *curr;
	t_list *next;

	if (!alst)
		return ;
	curr = *alst;
	while (curr)
	{
		next = curr->next;
		if (del)
			del(curr->content, curr->content_size);
		free(curr);
		curr = next;
	}
	*alst = NULL;
}

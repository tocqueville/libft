VPATH += lst/
SRC_LST = ft_lstadd.c ft_lstdel.c ft_lstdelone.c ft_lstiter.c ft_lstmap.c \
	ft_lstnew.c ft_lstsort.c

OBJ += $(SRC_LST:.c=.o)
HEADER += lst/ft_list.h

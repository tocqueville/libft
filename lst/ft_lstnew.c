/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:07:52 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:08:51 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include "../mem/ft_memory.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list *elem;

	if ((elem = malloc(sizeof(t_list))))
	{
		if (content)
		{
			if ((elem->content = malloc(content_size)))
			{
				ft_memcpy(elem->content, content, content_size);
				elem->content_size = content_size;
			}
			else
			{
				elem->content_size = 0;
			}
		}
		else
		{
			elem->content = NULL;
			elem->content_size = 0;
		}
		elem->next = NULL;
	}
	return (elem);
}

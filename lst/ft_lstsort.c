/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:07:52 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/12 09:09:01 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

static void	lst_to_head(t_list *prev, t_list *elem, t_list **begin)
{
	t_list *first;
	t_list *second;

	if (prev)
	{
		first = *begin;
		second = first->next;
		first->next = elem->next;
		if (second == elem)
			elem->next = first;
		else
		{
			elem->next = second;
			prev->next = first;
		}
		*begin = elem;
	}
}

void		ft_lstsort(t_list **alst, int (*cmp)(t_list *, t_list *))
{
	t_list *pi;
	t_list *i;
	t_list *pmin;
	t_list *min;

	if (!alst || !*alst)
		return ;
	pi = 0;
	i = *alst;
	pmin = 0;
	min = i;
	while (i)
	{
		if ((*cmp)(i, min) < 0)
		{
			pmin = pi;
			min = i;
		}
		pi = i;
		i = i->next;
	}
	lst_to_head(pmin, min, alst);
	ft_lstsort(&(*alst)->next, cmp);
}

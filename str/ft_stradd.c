/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stradd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:37:00 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 09:37:00 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

int	ft_stradd(char **res, const char *str)
{
	char *new;

	if (str == NULL)
		return (0);
	if (!*res)
		new = ft_strdup(str);
	else
		new = ft_strjoin(*res, str);
	free(*res);
	*res = new;
	return (new == NULL);
}

int		ft_strpush(char **res, char c)
{
	char tmp[2];

	tmp[0] = c;
	tmp[1] = 0;
	return (ft_stradd(res, tmp));
}

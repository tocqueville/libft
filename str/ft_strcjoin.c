/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:37:00 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 13:49:28 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_strcjoin(char const *s1, const char c, char const *s2)
{
	unsigned int	i;
	char			*join;

	if (!s1 || !s2)
		return (NULL);
	if (!(join = ft_strnew(ft_strlen(s1) + 1 + ft_strlen(s2))))
		return (NULL);
	i = 0;
	while (*s1)
		join[i++] = *s1++;
	join[i++] = c;
	while (*s2)
		join[i++] = *s2++;
	join[i] = 0;
	return (join);
}

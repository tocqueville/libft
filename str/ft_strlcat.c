/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:37:01 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 13:51:57 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

size_t	ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t	i;
	size_t	j;
	size_t	ld;
	size_t	ls;

	ld = ft_strlen(dest);
	ls = ft_strlen(src);
	if (size < ld)
		return (ls + size);
	i = 0;
	j = ld;
	while (j < size - 1 && src[i])
	{
		dest[j] = src[i];
		j++;
		i++;
	}
	dest[j] = 0;
	return (ls + ld);
}

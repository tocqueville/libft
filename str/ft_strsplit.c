/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:37:01 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 13:53:03 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

static int	count_word(char *str, char c)
{
	int l;

	l = 0;
	while (*str)
	{
		while (*str && *str == c)
			str++;
		if (!*str)
			return (l);
		l++;
		while (*str && *str != c)
			str++;
	}
	return (l);
}

static char	*next_word(char *str, int *size, char c)
{
	char *start;

	*size = 0;
	while (*str && *str == c)
		str++;
	if (!*str)
		return (str);
	start = str;
	while (*str && *str != c)
		str++;
	*size = str - start;
	return (start);
}

static char	**free_res(char **res)
{
	int i;

	i = 0;
	while (res[i])
		free(res[i++]);
	free(res);
	return (NULL);
}

static char	**do_split(char *str, char c, int limits)
{
	int		i;
	int		j;
	int		size;
	char	**res;

	if (!(res = malloc(sizeof(char *) * (limits + 1))))
		return (NULL);
	i = 0;
	while (i < limits)
	{
		str = next_word(str, &size, c);
		if (!(res[i] = (char *)malloc(size + 1)))
			return (free_res(res));
		j = -1;
		while (++j < size)
			res[i][j] = str[j];
		res[i++][size] = 0;
		str += size;
	}
	res[limits] = 0;
	return (res);
}

char		**ft_strsplit(char const *s, char c)
{
	if (!s)
		return (NULL);
	return (do_split((char*)s, c, count_word((char*)s, c)));
}

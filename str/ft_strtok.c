/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtok.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 13:59:36 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 13:59:37 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"
#include "../chr/ft_char.h"
#include "../mem/ft_memory.h"

static void	update_stacks(size_t stacks[3], char c)
{
	if (c == '(')
		stacks[0]++;
	else if (c == ')')
		stacks[0]--;
	else if (c == '\'')
		stacks[1]++;
	else if (c == '\"')
		stacks[2]++;
}

char		*ft_strtok(char **str)
{
	size_t	stacks[3];
	char	*start;

	ft_memset(stacks, 0, sizeof(stacks));
	while (ft_isspace(**str))
		(*str)++;
	if (!**str)
		return (NULL);
	start = *str;
	while (**str)
	{
		if (ft_isspace(**str))
		{
			if (!stacks[0] && !(stacks[1] % 2) && !(stacks[2] % 2))
				return (start);
		}
		else
			update_stacks(stacks, **str);
		(*str)++;
	}
	if (!stacks[0] && !(stacks[1] % 2) && !(stacks[2] % 2))
		return (start);
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrtokenize.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 09:04:54 by agrouard          #+#    #+#             */
/*   Updated: 2018/12/12 10:57:28 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"
#include "../arr/ft_array.h"

static char	*next_token(char *str, char c)
{
	while (*str == c)
		str++;
	return (str);
}

static char	*parse_token(char *str, char c)
{
	while (*str)
	{
		if (*str == c)
			return (str);
		str++;
	}
	return (str);
}

t_array		*ft_strsplitarr(char *str, char c)
{
	t_array	*res;
	char	*start;
	char	*end;

	if (!(res = ft_arrnew(4, free)))
		return (NULL);
	while (*str)
	{
		start = next_token(str, c);
		end = parse_token(start, c);
		if (start == end)
			break ;
		if (ft_arrpush(res, ft_strsub(start, 0, end - start)))
		{
			ft_arrfree(res);
			return (NULL);
		}
		str = end;
	}
	return (res);
}

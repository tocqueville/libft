/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:37:01 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 12:01:38 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strnstr(char *haystack, const char *needle, size_t n)
{
	size_t		n2;
	char		*str;
	const char	*to_find;

	if (*needle == '\0')
		return (haystack);
	while (n != 0 && *haystack)
	{
		str = haystack;
		to_find = needle;
		n2 = n;
		while (*str == *to_find)
		{
			if (--n2 == 0 && *(to_find + 1))
				return (NULL);
			if (!*++to_find)
				return (haystack);
			if (!*++str)
				return (NULL);
		}
		haystack++;
		n--;
	}
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:37:01 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 09:37:01 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strstr(const char *haystack, const char *needle)
{
	char		*hs;
	char		*str;
	const char	*to_find;

	hs = (char *)haystack;
	if (*needle == '\0')
		return (hs);
	while (*hs)
	{
		str = hs;
		to_find = needle;
		while (*str == *to_find)
		{
			str++;
			to_find++;
			if (!*to_find)
				return (hs);
			if (!*str)
				break ;
		}
		hs++;
	}
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:37:01 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/07 13:53:34 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

static int	isspace(int c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}

static char	*do_trim(size_t first_char, size_t last_char, char const *s)
{
	size_t	i;
	char	*trimmed;

	if (!(trimmed = malloc(last_char - first_char + 1)))
		return (NULL);
	i = first_char;
	while (i < last_char)
	{
		trimmed[i - first_char] = s[i];
		i++;
	}
	trimmed[i - first_char] = 0;
	return (trimmed);
}

char		*ft_strtrim(char const *s)
{
	size_t	first_char;
	size_t	last_char;
	char	*trimmed;

	if (!s)
		return (NULL);
	first_char = 0;
	while (isspace(s[first_char]))
		first_char++;
	last_char = ft_strlen(s);
	if (last_char == 0)
	{
		if ((trimmed = malloc(1)))
			trimmed[0] = 0;
		return (trimmed);
	}
	while (isspace(s[last_char - 1]) && last_char - 1 > first_char)
		last_char--;
	return (do_trim(first_char, last_char, s));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrouard <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 08:40:58 by agrouard          #+#    #+#             */
/*   Updated: 2018/11/14 08:41:53 by agrouard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include "arr/ft_array.h"
# include "chr/ft_char.h"
# include "lst/ft_list.h"
# include "map/ft_map.h"
# include "mem/ft_memory.h"
# include "put/ft_put.h"
# include "str/ft_string.h"

# include "gnl/get_next_line.h"

int		ft_atoi(const char *nptr);
char	*ft_itoa(int n);

#endif
